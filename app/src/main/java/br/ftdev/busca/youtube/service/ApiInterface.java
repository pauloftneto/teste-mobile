package br.ftdev.busca.youtube.service;

import br.ftdev.busca.youtube.model.pojo.ModelDataDetalhesVideos;
import br.ftdev.busca.youtube.model.pojo.ModelDataVideos;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("search")
    Call<ModelDataVideos> getVideos(@Query("part") String part,
                                    @Query("q") String q,
                                    @Query("key") String key,
                                    @Query("pageToken") String pageToken);

    @GET("videos")
    Call<ModelDataDetalhesVideos> getEstastiticas(@Query("id") String id,
                                                         @Query("part") String part,
                                                         @Query("key") String key);

}
