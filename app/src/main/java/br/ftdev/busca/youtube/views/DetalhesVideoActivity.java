package br.ftdev.busca.youtube.views;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;

import java.util.ArrayList;
import java.util.List;

import br.ftdev.busca.youtube.R;
import br.ftdev.busca.youtube.model.pojo.Item;
import br.ftdev.busca.youtube.model.pojo.ModelDataDetalhesVideos;
import br.ftdev.busca.youtube.model.pojo.ModelDataVideos;
import br.ftdev.busca.youtube.model.pojo.Video;
import br.ftdev.busca.youtube.service.ApiInterface;
import br.ftdev.busca.youtube.service.RetrofitAPI;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetalhesVideoActivity extends AppCompatActivity implements YouTubePlayer.OnInitializedListener {

    public static final String VIDEO_ID = "video_id";

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.video_titulo)
    TextView videoTitulo;

    @BindView(R.id.video_visualizacao)
    TextView videoVisualizacao;

    @BindView(R.id.video_descricao)
    TextView videoDescricao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhes_video);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        YouTubePlayerSupportFragment frag =
                (YouTubePlayerSupportFragment) getSupportFragmentManager().findFragmentById(R.id.video_player);
        frag.initialize(getString(R.string.GOOGLE_API_KEY), this);

        videoDescricao.setMovementMethod(new ScrollingMovementMethod());
        buscarEstastisticaVideo();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void buscarEstastisticaVideo() {

        ApiInterface apiInterface = RetrofitAPI.getVideosAPI(getString(R.string.BASE_URL));

        Call<ModelDataDetalhesVideos> call = apiInterface.getEstastiticas(
                getIntent().getStringExtra(VIDEO_ID),
                "snippet,statistics",
                getString(R.string.GOOGLE_API_KEY));

        call.enqueue(new Callback<ModelDataDetalhesVideos>() {
            @Override
            public void onResponse(Call<ModelDataDetalhesVideos> call, Response<ModelDataDetalhesVideos> response) {

                ModelDataDetalhesVideos modelDataDetalhesVideos = response.body();
                List<Video> resultadoBusca = modelDataDetalhesVideos.getItems();

                for (Video video : resultadoBusca) {
                    videoTitulo.setText(video.getSnippet().getTitle());
                    videoVisualizacao.setText(video.getStatistics().getViewCount() + " " + getString(R.string.visualizacoes));
                    videoDescricao.setText(video.getSnippet().getDescription());
                }

            }

            @Override
            public void onFailure(Call<ModelDataDetalhesVideos> call, Throwable t) {
                Log.d("DetalhesVideoActivity", t.toString());
            }
        });

    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player,
                                        boolean wasRestored) {
        if (!wasRestored) {
            //I assume the below String value is your video id
            player.cueVideo(getIntent().getStringExtra(VIDEO_ID));
        }

    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        Toast.makeText(this, getString(R.string.falha_youtube_player), Toast.LENGTH_LONG).show();
    }
}
