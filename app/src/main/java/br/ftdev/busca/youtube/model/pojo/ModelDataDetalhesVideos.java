package br.ftdev.busca.youtube.model.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ModelDataDetalhesVideos {

    @SerializedName("items")
    @Expose
    private List<Video> items = null;

    public List<Video> getItems() {
        return items;
    }

    public void setItems(List<Video> items) {
        this.items = items;
    }
}