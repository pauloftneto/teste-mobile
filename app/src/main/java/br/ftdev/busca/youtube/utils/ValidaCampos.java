package br.ftdev.busca.youtube.utils;

import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;


public class ValidaCampos {

    public static boolean validateNotNull(View ltView, View etView, String pMessage) {
        TextInputLayout ldView = (TextInputLayout) ltView;

        EditText edText = (EditText) etView;
        Editable text = edText.getText();

        if (text != null) {
            String strText = text.toString();
            if (TextUtils.isEmpty(strText)) {
                edText.requestFocus();
                ldView.setError(pMessage);

                return false;
            } else {
                ldView.setError(null);
                ldView.setErrorEnabled(false);

                return true;
            }

        }
        return false;
    }
}