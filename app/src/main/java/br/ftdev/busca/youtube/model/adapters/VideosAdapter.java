package br.ftdev.busca.youtube.model.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import br.ftdev.busca.youtube.R;
import br.ftdev.busca.youtube.model.pojo.Item;
import br.ftdev.busca.youtube.views.DetalhesVideoActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

import static android.support.v7.content.res.AppCompatResources.getDrawable;


public class VideosAdapter extends
        RecyclerView.Adapter<VideosAdapter.ViewHolder> {

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.video_view)
        ConstraintLayout videoView;

        @BindView(R.id.video_thumbnail)
        ImageView videoThumbnail;

        @BindView(R.id.video_titulo)
        TextView videoTitulo;

        @BindView(R.id.video_descricao)
        TextView videoDescricao;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    private ArrayList<Item> mVideos = new ArrayList<>();

    @NonNull
    @Override
    public VideosAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Infla layout personalizado
        View contactView = inflater.inflate(R.layout.adapter_videos, parent, false);

        // Retorna uma nova instancia do holder
        return new ViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(@NonNull final VideosAdapter.ViewHolder viewHolder, int position) {
        final Item video = mVideos.get(position);

        if (video.getSnippet().getThumbnails() != null) {
            Picasso.get()
                    .load(video.getSnippet().getThumbnails().getMedium().getUrl())
                    .error(R.drawable.placeholder_video)
                    .placeholder(R.drawable.placeholder_video)
                    .into(viewHolder.videoThumbnail);
        } else {
            viewHolder.videoThumbnail.setImageResource(R.drawable.placeholder_video);
        }

        viewHolder.videoTitulo.setText(video.getSnippet().getTitle());
        viewHolder.videoDescricao.setText(video.getSnippet().getDescription());

        viewHolder.videoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (video.getId() != null && video.getId().getVideoId() != null) {
                    Intent intent = new Intent(viewHolder.itemView.getContext(), DetalhesVideoActivity.class);

                    intent.putExtra(DetalhesVideoActivity.VIDEO_ID, video.getId().getVideoId());

                    viewHolder.itemView.getContext().startActivity(intent);
                }else {
                    Snackbar snackbar = Snackbar.make(viewHolder.itemView.getRootView(),
                            R.string.falha_video_id,
                            Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mVideos.size();
    }

    public void addAll(ArrayList<Item> videos) {
        int currentVideoCount = this.mVideos.size();
        this.mVideos.addAll(videos);
        notifyItemRangeInserted(currentVideoCount, videos.size());
    }

    public void replaceWith(ArrayList<Item> videos) {
        if (videos != null) {
            int oldCount = this.mVideos.size();
            int newCount = videos.size();
            int delCount = oldCount - newCount;
            this.mVideos.clear();
            this.mVideos.addAll(videos);
            if (delCount > 0) {
                notifyItemRangeChanged(0, newCount);
                notifyItemRangeRemoved(newCount, delCount);
            } else if (delCount < 0) {
                notifyItemRangeChanged(0, oldCount);
                notifyItemRangeInserted(oldCount, -delCount);
            } else {
                notifyItemRangeChanged(0, newCount);
            }
        }
    }

}
