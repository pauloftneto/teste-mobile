package br.ftdev.busca.youtube.views;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.design.button.MaterialButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import java.util.ArrayList;

import br.ftdev.busca.youtube.R;
import br.ftdev.busca.youtube.model.adapters.VideosAdapter;
import br.ftdev.busca.youtube.model.pojo.Item;
import br.ftdev.busca.youtube.model.pojo.ModelDataVideos;
import br.ftdev.busca.youtube.service.ApiInterface;
import br.ftdev.busca.youtube.service.RetrofitAPI;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static br.ftdev.busca.youtube.utils.ValidaCampos.validateNotNull;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.constraint_layout)
    ConstraintLayout constraintLayout;

    @BindView(R.id.pesquisar_layout)
    TextInputLayout pesquisaInputLayout;

    @BindView(R.id.pesquisar_edit_text)
    TextInputEditText pesquisaEditText;

    @BindView(R.id.videos_recycler_view)
    RecyclerView videosRecyclerView;

    @BindView(R.id.carregar_mais_button)
    MaterialButton carregaMaisButton;

    private VideosAdapter videosAdapter;
    private ProgressDialog mProgressDialog;
    private ArrayList<Item> resultadoBusca;

    private String lastToken = "";

    private boolean isAnim = false;
    private ConstraintSet layout1;
    private ConstraintSet layout2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        layout1 = new ConstraintSet();
        layout2 = new ConstraintSet();

        layout2.clone(this, R.layout.activity_main_result);
        layout1.clone(constraintLayout);

        pesquisaEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    buscarVideos();
                    return false;
                }
                return true;
            }
        });

        videosAdapter = new VideosAdapter();
        videosRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        videosRecyclerView.setAdapter(videosAdapter);
        videosAdapter.notifyDataSetChanged();

    }

    @OnClick(R.id.buscar_button)
    void buscarVideos() {
        if (validateNotNull(pesquisaInputLayout, pesquisaEditText,
                getString(R.string.valor_campo_obrigatorio))) {

            buscarVideosYouTube(pesquisaEditText.getText().toString(), false);

            showProgressDialog(getString(R.string.buscar_videos) + " " + pesquisaEditText.getText().toString());

            hideKeyboard();
        }

    }

    @OnClick(R.id.carregar_mais_button)
    void carregarMaisVideos() {
        buscarVideosYouTube(pesquisaEditText.getText().toString(), true);

        showProgressDialog(getString(R.string.buscar_mais_videos) + " " + pesquisaEditText.getText().toString());
    }

    private void showProgressDialog(String mensagem) {
        dismissProgressDialog();
        mProgressDialog = ProgressDialog.show(this, null, mensagem, true, false);
    }

    private void dismissProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    private void buscarVideosYouTube(String keywords, final boolean more) {

        if (!more) {
            lastToken = "";
        }

        ApiInterface apiInterface = RetrofitAPI.getVideosAPI(getString(R.string.BASE_URL));

        Call<ModelDataVideos> call = apiInterface.getVideos("snippet",
                keywords,
                getString(R.string.GOOGLE_API_KEY),
                lastToken);

        call.enqueue(new Callback<ModelDataVideos>() {
            @Override
            public void onResponse(Call<ModelDataVideos> call, Response<ModelDataVideos> response) {

                dismissProgressDialog();

                ModelDataVideos modelDataVideos = response.body();
                if (modelDataVideos != null) {

                    resultadoBusca = (ArrayList<Item>) modelDataVideos.getItems();

                    if (!resultadoBusca.isEmpty()) {

                        iniciaAnimacao();

                        lastToken = modelDataVideos.getNextPageToken();

                        if (more) {
                            videosAdapter.addAll(resultadoBusca);
                        } else {
                            videosAdapter.replaceWith(resultadoBusca);
                        }

                        carregaMaisButton.setVisibility(View.VISIBLE);
                    } else {
                        isAnim = true;
                        Snackbar snackbar = Snackbar.make(constraintLayout,
                                R.string.falha_encontrar_video,
                                Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }


                }

            }

            @Override
            public void onFailure(Call<ModelDataVideos> call, Throwable t) {
                Log.d("MainActivity", t.toString());
            }
        });

    }

    @OnClick(R.id.image_view)
    void resetaAnimacao() {
        if (isAnim) {
            TransitionManager.beginDelayedTransition(constraintLayout);
            layout1.applyTo(constraintLayout);
            isAnim = !isAnim;

            pesquisaEditText.setText(null);
            pesquisaInputLayout.requestFocus();
            pesquisaInputLayout.isFocusableInTouchMode();

            resultadoBusca.clear();
            videosAdapter.replaceWith(resultadoBusca);
            hideKeyboard();

        }
    }

    private void iniciaAnimacao() {
        if (!isAnim) {
            TransitionManager.beginDelayedTransition(constraintLayout);
            layout2.applyTo(constraintLayout);
            isAnim = !isAnim;

            pesquisaEditText.requestFocus();
        }
    }

    private void hideKeyboard() {
        //esconde o teclado quando obtem uma resposta
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }
}
